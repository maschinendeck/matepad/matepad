<?php 
    $bindings = array();
    if ($currentUser) {
        $bindings["userName"] = $currentUser->name;
        $bindings["userAction"] = "?page=user_signout";
    } else {
        $bindings["userName"] = "log in";
        $bindings["userAction"] = "?page=user_signin";
    }

    $bindings["coffeinTotal"] = number_format(floatval($store->getStatistic()->coffeinSold), 0, ',', '.')."mg";

    bindAndRenderTemplate("cms_head.html", $bindings);
?>